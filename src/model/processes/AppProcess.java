package model.processes;

import java.util.List;

import dao.DAOMemoryProcess;

public class AppProcess {

	private String imageName;
	private Integer pid;
	private String sessionName;
	private Integer sessionNumber;
	private Integer memUsageKB;
	private int location;
	
	
	public AppProcess(String imageName, Integer pid, String sessionName, Integer sessionNumber,
			Integer memUsageKB) {
		
		this.imageName = imageName;
		this.pid = pid;
		this.sessionName = sessionName;
		this.sessionNumber = sessionNumber;
		this.memUsageKB = memUsageKB;
	}
	
	public AppProcess(String imageName, Integer pid, String sessionName, Integer sessionNumber,
			Integer memUsageKB, int location) {
	
	}	
		
	public Integer getMemUsageMB(){
		return memUsageKB/1024;
	}
	
	public Integer getMemUsageB(){
		return memUsageKB * 1024;
	}
	
	
	public Integer getMemUsageKB() {
		return memUsageKB;
	}



	public void setMemUsageKB(Integer memUsageKB) {
		this.memUsageKB = memUsageKB;
	}


	public int getLocation() {
		return location;
	}


	public void setLocation(int location) {
		this.location = location;
	}


	public String getImageName() {
		
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	public String getSessionName() {
		return sessionName;
	}
	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}
	public Integer getSessionNumber() {
		return sessionNumber;
	}
	public void setSessionNumber(Integer sessionNumber) {
		this.sessionNumber = sessionNumber;
	}
	
	@Override
	public String toString() {
		return "AppProcess [imageName=" + imageName + ", pid=" + pid + ", sessionName=" + sessionName
				+ ", sessionNumber=" + sessionNumber + ", memUsageAsString=" + memUsageKB + "]";
	}
	
	/**
	 * Metoda destinata transformarii unui obiect de tip AppProcess in format JSON
	 * @return
	 */
	public String toJSON(){
		return "{ imageName: '"+imageName+"', pid: "+pid+", sessionName: '"+sessionName+"', sessionNumber : "+sessionNumber+", memUsageAsString:"+memUsageKB+"}";
	}
	

	@SuppressWarnings({ "static-access", "unused" })
	public static void main(String[] args) {
		

		
		DAOMemoryProcess dm = new DAOMemoryProcess();
		List<AppProcess> procs = dm.getAllProcesses();
		
		System.out.println("==================================");
		for(AppProcess p : procs){
			// System.out.println(p);
		}
		
		AppProcess p2 = new AppProcess("System", 4, "Services", 0, procs.get(1).getMemUsageKB());
		
		if(procs.contains(p2)){
			System.out.println("PROCESUL ESTE PE LISTA");
		}else{
			System.out.println("NU E PE LISTA");
		}
		
		System.out.println("=======================");
		System.out.println("COMPARATIE INTRE DOUA PROCESE FAINE: ");
		System.out.println(procs.get(1));
		System.out.println(p2);
		
		System.out.println(p2 == procs.get(1));
		System.out.println(p2.equals(procs.get(1)));
	}
	
}
