package model.computer;

/**
 * Proprietatile de interes ale sistemului folosite in aplicatie
 * @author Bogdan
 *
 */
public enum EnumSystemProps {

	AdminPasswordStatus, TotalPhysicalMemory, Model, Name, SystemType, SystemSKUNumber, ThermalState, 
	NumberOfProcessors, NumberOfLogicalProcessors, PrimaryOwnerName, BootupState, PowerSupplyState 
}
