package model.computer;


public class Computer {

	
	private Integer id;
	private int totalPhysicalMemory;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getTotalPhysicalMemory() {
		return totalPhysicalMemory;
	}
	public void setTotalPhysicalMemory(int totalPhysicalMemory) {
		this.totalPhysicalMemory = totalPhysicalMemory;
	}
	
	
	
	
}
