package model.computer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class MSProperties {

	/**
	 * Metoda destinata citirii informatiilor referitoare la masina curenta. Proprietatile de interes sunt specificate in enum-ul <code>model.computer.EnumSystemProps.java</code>
	 * Informatiile sunt incarcate intr-un Map Java
	 * @return Map map ce contine informatiile referitoare la proprietatile sistemului
	 */
	public static Map<String, String> getMSSystemProps() {
		Map<String, String> props = new HashMap<>();
		try {
			for (EnumSystemProps numeProprietate : EnumSystemProps.values()) {
				Process p = Runtime.getRuntime().exec("wmic ComputerSystem get " + numeProprietate);
				String value = "";
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				while ((value = input.readLine()) != null) {
					if (!value.trim().equals("")) {
						props.put(String.valueOf(numeProprietate), value);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return props;

	}

	public static void main(String[] args) {
		Map<String, String> sysprops = getMSSystemProps();
		util.OSUtil.printMap(sysprops);
				
	}

}

