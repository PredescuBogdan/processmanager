-- MySQL dump 10.13  Distrib 5.7.11, for Win64 (x86_64)
--
-- Host: localhost    Database: projectmanager
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;



--
-- Table structure for table `computer`
--

DROP TABLE IF EXISTS `computer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `computer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NumberOfLogicalProcessors` int(11) DEFAULT NULL,
  `PowerSupplyState` int(11) DEFAULT NULL,
  `BootupState` varchar(240) DEFAULT NULL,
  `NumberOfProcessors` int(11) DEFAULT NULL,
  `Model` varchar(240) DEFAULT NULL,
  `TotalPhysicalMemory` varchar(240) DEFAULT NULL,
  `SystemType` varchar(240) DEFAULT NULL,
  `PrimaryOwnerName` varchar(240) DEFAULT NULL,
  `SystemSKUNumber` varchar(240) DEFAULT NULL,
  `AdminPasswordStatus` int(11) DEFAULT NULL,
  `Name` varchar(240) DEFAULT NULL,
  `ThermalState` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=398 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `computer`
--

LOCK TABLES `computer` WRITE;
/*!40000 ALTER TABLE `computer` DISABLE KEYS */;
INSERT INTO `computer` VALUES (397,8,3,'Normal boot',1,'N551JK','8467177472','x64-based PC','predescu.bogdan93@gmail.com','ASUS-NotebookSKU',3,'BOGDAN',3);
/*!40000 ALTER TABLE `computer` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `processes`
--

DROP TABLE IF EXISTS `processes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOCATION` int(11) DEFAULT NULL,
  `MEM_USAGE_MB` int(11) DEFAULT NULL,
  `SESSION_NAME` varchar(240) DEFAULT NULL,
  `IMAGE_NAME` varchar(240) DEFAULT NULL,
  `MEM_USAGE_B` int(11) DEFAULT NULL,
  `PID` int(11) DEFAULT NULL,
  `SESSION_NUMBER` int(11) DEFAULT NULL,
  `datemofidied` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=54411 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processes`
--

LOCK TABLES `processes` WRITE;
/*!40000 ALTER TABLE `processes` DISABLE KEYS */;
INSERT INTO `processes` VALUES (54275,NULL,0,'Services','System Idle Process',4096,0,0,'2016-07-28 01:04:57'),(54276,NULL,619,'Services','System',649142272,4,0,'2016-07-28 01:04:57'),(54277,NULL,0,'Services','smss.exe',159744,496,0,'2016-07-28 01:04:57'),(54278,NULL,1,'Services','csrss.exe',1474560,656,0,'2016-07-28 01:04:57'),(54279,NULL,0,'Services','wininit.exe',606208,764,0,'2016-07-28 01:04:57'),(54280,NULL,9,'Console','csrss.exe',9617408,772,1,'2016-07-28 01:04:57'),(54281,NULL,4,'Services','services.exe',5115904,840,0,'2016-07-28 01:04:57'),(54282,NULL,10,'Services','lsass.exe',10698752,848,0,'2016-07-28 01:04:57'),(54283,NULL,11,'Services','svchost.exe',12230656,944,0,'2016-07-28 01:04:57'),(54284,NULL,9,'Services','svchost.exe',9465856,1020,0,'2016-07-28 01:04:57'),(54285,NULL,0,'Console','winlogon.exe',913408,552,1,'2016-07-28 01:04:57'),(54286,NULL,50,'Console','dwm.exe',53301248,376,1,'2016-07-28 01:04:57'),(54287,NULL,10,'Services','svchost.exe',11476992,1032,0,'2016-07-28 01:04:57'),(54288,NULL,27,'Services','svchost.exe',28344320,1084,0,'2016-07-28 01:04:57'),(54289,NULL,13,'Services','svchost.exe',14548992,1140,0,'2016-07-28 01:04:57'),(54290,NULL,2,'Services','svchost.exe',3076096,1228,0,'2016-07-28 01:04:57'),(54291,NULL,3,'Services','nvvsvc.exe',3162112,1256,0,'2016-07-28 01:04:57'),(54292,NULL,1,'Services','igfxCUIService.exe',1945600,1340,0,'2016-07-28 01:04:57'),(54293,NULL,15,'Services','svchost.exe',15937536,1400,0,'2016-07-28 01:04:57'),(54294,NULL,13,'Services','svchost.exe',13791232,1504,0,'2016-07-28 01:04:57'),(54295,NULL,3,'Console','nvxdsync.exe',3735552,1528,1,'2016-07-28 01:04:57'),(54296,NULL,1,'Console','nvvsvc.exe',1617920,1536,1,'2016-07-28 01:04:57'),(54297,NULL,10,'Services','svchost.exe',11292672,1740,0,'2016-07-28 01:04:57'),(54298,NULL,1,'Services','AsLdrSrv.exe',1208320,1856,0,'2016-07-28 01:04:57'),(54299,NULL,1,'Services','IntelCpHeciSvc.exe',1265664,1876,0,'2016-07-28 01:04:57'),(54300,NULL,6,'Services','spoolsv.exe',6844416,2000,0,'2016-07-28 01:04:57'),(54301,NULL,1,'Services','armsvc.exe',1564672,2172,0,'2016-07-28 01:04:57'),(54302,NULL,3,'Services','HD-UpdaterService.exe',3158016,2180,0,'2016-07-28 01:04:57'),(54303,NULL,2,'Services','mDNSResponder.exe',2551808,2196,0,'2016-07-28 01:04:57'),(54304,NULL,5,'Services','AppleMobileDeviceService.',6033408,2216,0,'2016-07-28 01:04:57'),(54305,NULL,18,'Services','svchost.exe',19546112,2280,0,'2016-07-28 01:04:57'),(54306,NULL,2,'Services','DriverMFTService.exe',2146304,2324,0,'2016-07-28 01:04:57'),(54307,NULL,2,'Services','Droid4XService.exe',2916352,2332,0,'2016-07-28 01:04:57'),(54308,NULL,2,'Services','ETDService.exe',2428928,2532,0,'2016-07-28 01:04:57'),(54309,NULL,1,'Services','GfExperienceService.exe',1912832,2540,0,'2016-07-28 01:04:57'),(54310,NULL,1,'Services','ibtsiva.exe',1171456,2548,0,'2016-07-28 01:04:57'),(54311,NULL,1,'Services','NvNetworkService.exe',1724416,2696,0,'2016-07-28 01:04:57'),(54312,NULL,18,'Services','mysqld.exe',19030016,2732,0,'2016-07-28 01:04:57'),(54313,NULL,1,'Services','NvStreamService.exe',1957888,2820,0,'2016-07-28 01:04:57'),(54314,NULL,150,'Services','MsMpEng.exe',157798400,2844,0,'2016-07-28 01:04:57'),(54315,NULL,1,'Services','ss_conn_service.exe',1523712,2876,0,'2016-07-28 01:04:57'),(54316,NULL,11,'Services','svchost.exe',11866112,2904,0,'2016-07-28 01:04:57'),(54317,NULL,1,'Services','svchost.exe',1667072,2912,0,'2016-07-28 01:04:57'),(54318,NULL,2,'Services','TeamViewer_Service.exe',2482176,3028,0,'2016-07-28 01:04:57'),(54319,NULL,0,'Services','dasHost.exe',1003520,2460,0,'2016-07-28 01:04:57'),(54320,NULL,3,'Services','NvStreamNetworkService.ex',3837952,4020,0,'2016-07-28 01:04:57'),(54321,NULL,1,'Services','svchost.exe',1241088,4068,0,'2016-07-28 01:04:57'),(54322,NULL,15,'Services','WmiPrvSE.exe',15937536,4196,0,'2016-07-28 01:04:57'),(54323,NULL,0,'Services','NisSrv.exe',770048,4436,0,'2016-07-28 01:04:57'),(54324,NULL,1,'Console','HControl.exe',1286144,4572,1,'2016-07-28 01:04:57'),(54325,NULL,1,'Console','ETDCtrl.exe',1335296,4900,1,'2016-07-28 01:04:57'),(54326,NULL,4,'Console','NvStreamUserAgent.exe',4374528,4920,1,'2016-07-28 01:04:57'),(54327,NULL,0,'Console','conhost.exe',860160,4908,1,'2016-07-28 01:04:57'),(54328,NULL,19,'Console','sihost.exe',19976192,2488,1,'2016-07-28 01:04:57'),(54329,NULL,14,'Console','taskhostw.exe',15638528,5052,1,'2016-07-28 01:04:57'),(54330,NULL,0,'Console','USBChargerPlus.exe',102400,5012,1,'2016-07-28 01:04:57'),(54331,NULL,2,'Services','PresentationFontCache.exe',2461696,2584,0,'2016-07-28 01:04:57'),(54332,NULL,0,'Console','ACMON.exe',81920,5168,1,'2016-07-28 01:04:57'),(54333,NULL,0,'Console','SkypeHost.exe',90112,5452,1,'2016-07-28 01:04:57'),(54334,NULL,46,'Console','RuntimeBroker.exe',49242112,5508,1,'2016-07-28 01:04:57'),(54335,NULL,0,'Console','DMedia.exe',1040384,5692,1,'2016-07-28 01:04:57'),(54336,NULL,1,'Console','ATKOSD2.exe',1216512,5696,1,'2016-07-28 01:04:57'),(54337,NULL,1,'Console','igfxEM.exe',1155072,5944,1,'2016-07-28 01:04:57'),(54338,NULL,0,'Console','igfxHK.exe',1011712,6032,1,'2016-07-28 01:04:57'),(54339,NULL,6,'Console','NvBackend.exe',6782976,520,1,'2016-07-28 01:04:57'),(54340,NULL,52,'Services','SearchIndexer.exe',54910976,6364,0,'2016-07-28 01:04:57'),(54341,NULL,1,'Console','AsusTPLoader.exe',1929216,7076,1,'2016-07-28 01:04:57'),(54342,NULL,0,'Console','AsusTPCenter.exe',1040384,7108,1,'2016-07-28 01:04:57'),(54343,NULL,0,'Console','nvtray.exe',937984,7244,1,'2016-07-28 01:04:57'),(54344,NULL,0,'Console','AsusTPHelper.exe',126976,7452,1,'2016-07-28 01:04:57'),(54345,NULL,0,'Console','SettingSyncHost.exe',655360,7500,1,'2016-07-28 01:04:57'),(54346,NULL,2,'Console','DropboxUpdate.exe',3014656,7556,1,'2016-07-28 01:04:57'),(54347,NULL,10,'Console','MySQLNotifier.exe',11227136,7756,1,'2016-07-28 01:04:57'),(54348,NULL,4,'Services','WmiPrvSE.exe',4558848,8016,0,'2016-07-28 01:04:57'),(54349,NULL,32,'Console','Dropbox.exe',33910784,7976,1,'2016-07-28 01:04:57'),(54350,NULL,0,'Console','RAVBg64.exe',585728,8596,1,'2016-07-28 01:04:57'),(54351,NULL,0,'Console','RAVCpl64.exe',569344,8604,1,'2016-07-28 01:04:57'),(54352,NULL,1,'Services','jhi_service.exe',1286144,1944,0,'2016-07-28 01:04:57'),(54353,NULL,1,'Services','LMS.exe',1994752,8332,0,'2016-07-28 01:04:57'),(54354,NULL,3,'Services','sppsvc.exe',3686400,3132,0,'2016-07-28 01:04:57'),(54355,NULL,10,'Console','svchost.exe',10850304,8532,1,'2016-07-28 01:04:57'),(54356,NULL,2,'Console','RemindersServer.exe',2224128,8848,1,'2016-07-28 01:04:57'),(54357,NULL,19,'Console','ApplicationFrameHost.exe',20520960,8644,1,'2016-07-28 01:04:57'),(54358,NULL,0,'Console','Video.UI.exe',294912,860,1,'2016-07-28 01:04:57'),(54359,NULL,2,'Console','MEmuConsole.exe',2990080,6880,1,'2016-07-28 01:04:57'),(54360,NULL,14,'Services','audiodg.exe',15589376,7752,0,'2016-07-28 01:04:57'),(54361,NULL,18,'Console','LiveUpdate.exe',19603456,4984,1,'2016-07-28 01:04:57'),(54362,NULL,0,'Console','SystemSettings.exe',32768,9072,1,'2016-07-28 01:04:57'),(54363,NULL,0,'Console','fontdrvhost.exe',487424,12696,1,'2016-07-28 01:04:57'),(54364,NULL,9,'Console','OneDrive.exe',9469952,8048,1,'2016-07-28 01:04:57'),(54365,NULL,13,'Console','HD-Agent.exe',13869056,14040,1,'2016-07-28 01:04:57'),(54366,NULL,3,'Services','HD-LogRotatorService.exe',3878912,11464,0,'2016-07-28 01:04:57'),(54367,NULL,5,'Services','HD-Plus-Service.exe',6144000,14228,0,'2016-07-28 01:04:57'),(54368,NULL,2,'Services','BstkSVC.exe',2998272,13440,0,'2016-07-28 01:04:57'),(54369,NULL,2,'Console','adb.exe',2334720,7908,1,'2016-07-28 01:04:57'),(54370,NULL,6,'Console','taskhostw.exe',7225344,13892,1,'2016-07-28 01:04:57'),(54371,NULL,116,'Console','chrome.exe',121901056,11888,1,'2016-07-28 01:04:57'),(54372,NULL,1,'Console','chrome.exe',1380352,4064,1,'2016-07-28 01:04:57'),(54373,NULL,55,'Console','chrome.exe',57671680,12244,1,'2016-07-28 01:04:57'),(54374,NULL,6,'Console','chrome.exe',7139328,12416,1,'2016-07-28 01:04:57'),(54375,NULL,11,'Console','chrome.exe',11882496,13304,1,'2016-07-28 01:04:57'),(54376,NULL,95,'Console','explorer.exe',99758080,14276,1,'2016-07-28 01:04:57'),(54377,NULL,34,'Console','ShellExperienceHost.exe',35815424,13340,1,'2016-07-28 01:04:57'),(54378,NULL,445,'Console','chrome.exe',466915328,12524,1,'2016-07-28 01:04:57'),(54379,NULL,14,'Console','chrome.exe',15269888,43548,1,'2016-07-28 01:04:57'),(54380,NULL,16,'Console','chrome.exe',17629184,62244,1,'2016-07-28 01:04:57'),(54381,NULL,4,'Console','ApplePhotoStreams.exe',5206016,64432,1,'2016-07-28 01:04:57'),(54382,NULL,2,'Console','APSDaemon.exe',2940928,64116,1,'2016-07-28 01:04:57'),(54383,NULL,4,'Console','MEmuSVC.exe',4616192,105288,1,'2016-07-28 01:04:57'),(54384,NULL,128,'Console','MEmu.exe',134971392,105448,1,'2016-07-28 01:04:57'),(54385,NULL,12,'Console','MEmuHeadless.exe',12607488,76836,1,'2016-07-28 01:04:57'),(54386,NULL,0,'Console','conhost.exe',831488,76860,1,'2016-07-28 01:04:57'),(54387,NULL,82,'Console','Microsoft.Photos.exe',86179840,118788,1,'2016-07-28 01:04:57'),(54388,NULL,31,'Console','Calculator.exe',33542144,118940,1,'2016-07-28 01:04:57'),(54389,NULL,50,'Console','MyBot.run.exe',53305344,114532,1,'2016-07-28 01:04:57'),(54390,NULL,52,'Console','SearchUI.exe',54693888,133056,1,'2016-07-28 01:04:57'),(54391,NULL,96,'Console','chrome.exe',100884480,129528,1,'2016-07-28 01:04:57'),(54392,NULL,586,'Console','eclipse.exe',615497728,166108,1,'2016-07-28 01:04:57'),(54393,NULL,25,'Console','chrome.exe',26349568,166792,1,'2016-07-28 01:04:57'),(54394,NULL,24,'Console','notepad++.exe',25260032,167492,1,'2016-07-28 01:04:57'),(54395,NULL,35,'Console','chrome.exe',37535744,162128,1,'2016-07-28 01:04:57'),(54396,NULL,34,'Console','chrome.exe',35934208,161128,1,'2016-07-28 01:04:57'),(54397,NULL,65,'Console','chrome.exe',69111808,161376,1,'2016-07-28 01:04:57'),(54398,NULL,28,'Console','chrome.exe',30195712,162304,1,'2016-07-28 01:04:57'),(54399,NULL,44,'Console','chrome.exe',47181824,167096,1,'2016-07-28 01:04:57'),(54400,NULL,29,'Console','chrome.exe',30752768,166816,1,'2016-07-28 01:04:57'),(54401,NULL,45,'Console','chrome.exe',47923200,165520,1,'2016-07-28 01:04:57'),(54402,NULL,213,'Console','javaw.exe',223674368,168844,1,'2016-07-28 01:04:57'),(54403,NULL,36,'Console','chrome.exe',38584320,160540,1,'2016-07-28 01:04:57'),(54404,NULL,4,'Console','adb.exe',5115904,168320,1,'2016-07-28 01:04:57'),(54405,NULL,6,'Console','conhost.exe',6569984,168604,1,'2016-07-28 01:04:57'),(54406,NULL,122,'Console','chrome.exe',128233472,168744,1,'2016-07-28 01:04:57'),(54407,NULL,9,'Services','SearchProtocolHost.exe',9945088,150296,0,'2016-07-28 01:04:57'),(54408,NULL,8,'Services','SearchFilterHost.exe',8491008,8260,0,'2016-07-28 01:04:57'),(54409,NULL,7,'Console','tasklist.exe',7348224,140100,1,'2016-07-28 01:04:57'),(54410,NULL,5,'Console','conhost.exe',5697536,168500,1,'2016-07-28 01:04:57');
/*!40000 ALTER TABLE `processes` ENABLE KEYS */;
UNLOCK TABLES;


/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-15 19:02:22
