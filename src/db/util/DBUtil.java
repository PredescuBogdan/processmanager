package db.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class DBUtil {

	/**
	 * Metoda destinata crearii tabelelor de la zero. Poate crea orice tabel. 
	 * @param tableName numele tabelului ce va fi creat
	 * @param numericIdColumnName numele cheii primare 
	 * @param columnsAndTypes
	 * @return
	 */
	public static String queryBuilderCreateTable(String tableName, String numericIdColumnName, Map<String, String> columnsAndTypes){
		String query = "CREATE TABLE "+tableName+"("+numericIdColumnName+" int primary key auto_increment, ";
		String coltypes = "";
		
		Iterator<Entry<String, String>> it = columnsAndTypes.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, String> pair = it.next();
			coltypes += pair.getKey() + " " + pair.getValue() + ",";
		}
		System.out.println("TEST: " + coltypes);
		coltypes = coltypes.substring(0, coltypes.length()-1);
		query += coltypes;
		query +=  ")";
		return query;
	}
	
	/**
	 * Metoda destinata inserarii datelor in tabel.
	 * @param tableName numele tabelului in care se va face inserarea datelor
	 * @param colsValsMap Map-ul ce ulterior va fi parsat element cu element
	 * @return
	 */
	public static String queryBuilderInsert(String tableName, Map<String, String> colsValsMap){
		
		String query = "INSERT INTO "+tableName+"( ";
		String columns = "";
		String values = "";
		Iterator<Entry<String, String>> it = colsValsMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, String> pair = it.next();
			// coltypes += pair.getKey() + " " + pair.getValue() + ",";
			columns += pair.getKey()+" ,";
			values += "'"+pair.getValue()+"'"+" ,";
			
		}
		columns = columns.substring(0, columns.length()-1);
		values = values.substring(0, values.length()-1);
		query += columns;
		query +=  ")";
		query += "VALUES (" + values + ")";
		return query;
	}
	
}
