package db.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class MySQLConnectionProvider implements IConnectionProvider {

	private Connection conn = null;
	private Statement stmt = null;

	public static void main(String[] args) {
		MySQLConnectionProvider conprov = new MySQLConnectionProvider();
		conprov.incarcareProprietati();
	}

	/**
	 * In vederea implementarii transparente, detaliile de conectare sunt
	 * pastrate in <i>configuration.properties</i>
	 * 
	 * @return
	 */
	public Properties incarcareProprietati() {

		Properties prop = new Properties();
		InputStream input = null;

		try {
			input = this.getClass().getResourceAsStream("configuration.properties");
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;

	}

	/**
	 * Conexiunea este instiantiata in constructor pe baza informatiilor
	 * intoarse de metoda <code>incarcareProprietati()</code>
	 */
	public MySQLConnectionProvider() {
		try {

			System.out.println("CREATING CONNECTION");
			/*
			 * Class.forName("com.mysql.jdbc.Driver"); conn = (Connection)
			 * DriverManager.getConnection("jdbc:mysql://"+incarcareProprietati(
			 * ).getProperty("HOST")+":"+incarcareProprietati().getProperty(
			 * "PORT")+"/"+incarcareProprietati().getProperty("DB")+"",
			 * incarcareProprietati().getProperty("USER"),
			 * incarcareProprietati().getProperty("PASSWORD")); // java.sql stmt
			 * = (Statement) conn.createStatement();
			 */

			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc/PROJMAN");
			conn = ds.getConnection();
			stmt = conn.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Connection createConnection() {
		return conn;
	}

	public Statement createStatement() {
		return stmt;
	}

	public void setStmt(Statement stmt) {
		this.stmt = stmt;
	}

}
