package db.util;

import java.util.ArrayList;
import java.util.List;

import model.processes.AppProcess;

public class CacheDBProcess {
	
	/**
	 *  lista de liste de procese
	 * fiecare punct din lista va reprezenta starea proceselor la un anumit punct
	 * 		e.g. theCache.get(0) o lista de AppProcess la prima masuratoare
	 **/
	private List<List<AppProcess>> theCache;
	private static final int MAXIMUM_CACHE_VALUE = 30;
	
	public CacheDBProcess(){
		theCache = new ArrayList<>();
	}
	
	/**
	 * Cache-ul este updatat o data la <strong>MAXIMUM_CACHE_VALUE</strong> de masuratori
	 * @param snapshot
	 */
	public void addDbSnapshot(List<AppProcess> snapshot){
		if(theCache.size() > MAXIMUM_CACHE_VALUE){
			theCache.remove(0);
		}
		theCache.add(snapshot);
	}

	/**
	 * Metoda pentru aflarea starii proceselor reprezentate sub forma unei liste
	 * @param i reprezinta momentul masuratorii in interval
	 * @return lista de procese intoarse la punctul ti
	 */
	public List<AppProcess> getSnapshotAtInstance(int i){
		return theCache.get(i);
	}
	
	public int getCacheSize(){
		return theCache.size();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int indexSnapshot = 0;
		for(List<AppProcess> sh : theCache){
			sb.append("============================================================" + indexSnapshot + "============================================================\n\n");
			indexSnapshot++;
			sb.append(sh.toString()+"\n");
		}
		return sb.toString();
	}
	
	
	
}
