package db.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import dao.DAOMemoryProcess;
import model.computer.MSProperties;
import model.processes.AppProcess;

public class ScriptUtil {

	private IConnectionProvider connProv;
	
	private static final String TABLE_PROCESSES = "processes";
	private static final String TABLE_COMPUTER = "computer";
	
	public ScriptUtil() {
		connProv = new MySQLConnectionProvider();
		cleanDBDelete();
//		cleanDBDROP();
	}
	
	/**
	 * Metoda stergere randuri din baza de date
	 */
	public void cleanDBDelete(){
		Connection conn = connProv.createConnection();
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			String recreateQuery = "DELETE FROM "+TABLE_COMPUTER;
			stmt.execute(recreateQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			stmt.execute("DELETE FROM " +TABLE_PROCESSES);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metoda care curata baza de date (i.e. da drop tabelelor)
	 */
	public void cleanDBDROP() {
		Connection conn = connProv.createConnection();
		Statement stmt = null;
		try {

			stmt = conn.createStatement();
			String recreateQuery = "DROP TABLE "+TABLE_COMPUTER;
			stmt.execute(recreateQuery);
		} catch (Exception e) {
			System.out.println("TABLE ALREADY EXISTS... ");
		}
		try {
			stmt.execute("DROP TABLE " +TABLE_PROCESSES);
		} catch (Exception e) {
			System.out.println("TABLE MEMORY ALREADY EXISTS... ");
		}
	}

	/**
	 * Metoda care creaza si updateaza tabelul <strong>TABLE_PROCESSES</strong>
	 *  
	 * @param create specifica daca dorim recrearea tabelului (cazul in care aplicatia este rerulata dupa o inchidere anterioara)
	 */
	public void generateScriptsProcesses(boolean create){
		
		Connection conn = connProv.createConnection();
		Statement stmt = null;
		
		List<AppProcess> processes = DAOMemoryProcess.getAllProcesses();
		Map<String, String> mapValoriAppProcess = new HashMap<>();
		
		AppProcess temporaryObject = processes.get(0);
		mapValoriAppProcess.put("LOCATION", ""+temporaryObject.getLocation());
		mapValoriAppProcess.put("IMAGE_NAME", ""+temporaryObject.getImageName());
		mapValoriAppProcess.put("SESSION_NAME", ""+temporaryObject.getSessionName());
		mapValoriAppProcess.put("MEM_USAGE_B", ""+temporaryObject.getMemUsageB());
		mapValoriAppProcess.put("MEM_USAGE_MB", ""+temporaryObject.getMemUsageMB());
		mapValoriAppProcess.put("SESSION_NUMBER", ""+temporaryObject.getSessionNumber());
		mapValoriAppProcess.put("PID", ""+temporaryObject.getPid());
		
		// creare tabel processes 
		String queryCreate = DBUtil.queryBuilderCreateTable(TABLE_PROCESSES, "ID", getTableDataTypes(mapValoriAppProcess));
		
		try{
			stmt = conn.createStatement();
			if(create){
				stmt.execute(queryCreate);
				try{ // => adaugam coloana
					stmt.execute("ALTER TABLE " + TABLE_PROCESSES + " add column datemofidied datetime default now()"); 
				}catch(Exception e2){
					e2.printStackTrace();
				}
				
			}
		}catch(Exception e){  // daca tabelul exista => exceptie =>
			e.printStackTrace(); 
			try{ // => adaugam coloana
				stmt.execute("ALTER TABLE " + TABLE_PROCESSES + " add column datemofidied datetime default now()"); 
			}catch(Exception e2){
				e2.printStackTrace();
			}
		}
		
		
		for(AppProcess ap : processes){
			Map<String, String> colsAndValues = new HashMap<>();
			colsAndValues.put("IMAGE_NAME", ""+ap.getImageName());
			colsAndValues.put("SESSION_NAME", ""+ap.getSessionName());
			colsAndValues.put("MEM_USAGE_B", ""+ap.getMemUsageB());
			colsAndValues.put("MEM_USAGE_MB", ""+ap.getMemUsageMB());
			colsAndValues.put("PID", ""+ap.getPid());
			colsAndValues.put("SESSION_NUMBER", ""+ap.getSessionNumber());

			
			// inserare date in tabelul processes
			String queryInsertProcess = DBUtil.queryBuilderInsert(TABLE_PROCESSES, colsAndValues);
			try {
				stmt.executeUpdate(queryInsertProcess);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * 
	 * @param m obiect de tip Map<String, String> ce contine valori pe care le parsam in vederea stabilirii tipului de date
	 * @return map in care specificam fiecare coloana si tipul de date
	 */
	public Map<String, String> getTableDataTypes(Map<String, String> m){
		Map<String, String> tipuriDate = new HashMap<>();
		Iterator<Entry<String, String>> it = m.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<String, String> pair = it.next();
			boolean isNumber = true;
			try { 
				Integer.valueOf(pair.getValue().trim());
			} catch (Exception e) {
				isNumber = false;
			}
			if (isNumber) {
				tipuriDate.put(pair.getKey(), "INT");
			} else {
				tipuriDate.put(pair.getKey(), "VARCHAR(240)");
			}
		}
		return tipuriDate;
	}
	
	
	/**
	 * Metoda generare tabel <strong>TABLE_COMPUTER</strong> si popularea acestuia cu informatiile puse la dispozitie de DAOMemoryProcess
	 * @param create parametru reprezentat un boolean. true daca dorim crearea bazei de date, false daca dorim doar popularea acesteia
	 */
	public void generateDBScriptsComputers(boolean create) {

		try {
			Connection conn = connProv.createConnection();
			Statement stmt = conn.createStatement();

			Map<String, String> msSysProp = MSProperties.getMSSystemProps();

			Iterator<Entry<String, String>> it = msSysProp.entrySet().iterator();

			StringBuilder coloaneFaraTip = new StringBuilder();
			StringBuilder values = new StringBuilder();
			Map<String, String> createMap = getTableDataTypes(msSysProp);
			Map<String, String> insertMap = new HashMap<>();

			while (it.hasNext()) {
				Map.Entry<String, String> pair = it.next();
				System.out.println(pair.getKey() + " = " + pair.getValue());
				insertMap.put(pair.getKey(), pair.getValue().trim());
				coloaneFaraTip.append(pair.getKey() + ",");
				values.append("'" + pair.getValue().trim() + "',");
			}

			String queryCreate = DBUtil.queryBuilderCreateTable(TABLE_COMPUTER, "id", createMap); // "CREATE
																								// TABLE
																								// computer(id
																								// int
																								// primary
																								// key
																								// auto_increment,
																								// "
																								// +
																								// sirColoane
																								// +
																								// ")";
			System.out.println(queryCreate);

			String insertQuery = DBUtil.queryBuilderInsert(TABLE_COMPUTER, insertMap);
			System.out.println(insertQuery);
			insertQuery = insertQuery.replace('\\', ' '); // 

			if(create){
				stmt.execute(queryCreate);
			}
			stmt.executeUpdate(insertQuery);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		System.out.println("=======================================================");
		ScriptUtil su = new ScriptUtil();
		su.cleanDBDROP();
		su.generateDBScriptsComputers(true);
		su.generateScriptsProcesses(true);
		
		su.cleanDBDelete();
		
		su.generateDBScriptsComputers(false);
		su.generateScriptsProcesses(false);
		

	}
}
