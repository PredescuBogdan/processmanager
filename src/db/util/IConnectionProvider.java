package db.util;

import java.sql.Connection;
import java.sql.Statement;

public interface IConnectionProvider {

	public Connection createConnection();
	public Statement createStatement();
}
