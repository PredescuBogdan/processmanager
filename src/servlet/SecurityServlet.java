package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cron.DBRefreshCronJob;
import dao.DAOMemoryProcess;
import db.util.CacheDBProcess;
import db.util.ScriptUtil;
import model.computer.MSProperties;
import model.processes.AppProcess;
import util.ComputerOperationUtil;
import util.DebugUtil;



/**
 * Servlet implementation class SecurityServlet
 */
@WebServlet("/SecurityServlet")
public class SecurityServlet extends HttpServlet {
	
	
	
	private static final long serialVersionUID = 1L;
    public List<AppProcess> allProcesses = new ArrayList<>();
	public Map<String,String> allServers = new HashMap<>();
//    private DAOMySQLProcess dao = null;
	private ComputerOperationUtil cou = null;
	private DBRefreshCronJob dbjob = null;
	public Map<String,String> compDetails;
	
	
	
    @Override
	public void init() throws ServletException {
		super.init();
		dbjob = new DBRefreshCronJob(new DAOMemoryProcess());
		dbjob.startDBandCPURefreshing();
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public SecurityServlet() {
    	cou = new ComputerOperationUtil();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		if (request.getParameter("path").equals("security")) {
				request.setAttribute("msg", "ACCESS GRANTED");
				ScriptUtil su = new ScriptUtil();
				su.cleanDBDROP();
				su.generateDBScriptsComputers(true);
				su.generateScriptsProcesses(true);
				
				addInfoToMainPage(request);
				
				request.setAttribute("allServers", allServers);
				request.setAttribute("CACHE", dbjob.getCache());
				request.getRequestDispatcher("taskmanager.jsp").forward(request, response);
				
		}else if(request.getParameter("path").trim().equals("compdetails")) {
			compDetails = MSProperties.getMSSystemProps();
			request.setAttribute("ComputerInfo", compDetails);
			request.getRequestDispatcher("ComputerDetails.jsp").forward(request, response);
		}
		
		else if(request.getParameter("path").trim().equals("testchart")){
			
			request.setAttribute("CPU_CACHE",  dbjob.getCacheCPU());

			request.getRequestDispatcher("testcomp/testchart.jsp").forward(request, response);
		}else if(request.getParameter("path").trim().equals("allcharts")){
			
			response.sendRedirect("testcomp/allcharts.jsp");
		}
		
		else if(request.getParameter("path").trim().equals("process_graph")){
			DebugUtil.log("TEST 1");
			CacheDBProcess cache = dbjob.getCache();
			Integer from = null;
			Integer to = null;
			try{
				from = Integer.parseInt(request.getParameter("from"));
				to = Integer.parseInt(request.getParameter("to"));
			}catch(Exception e){
				e.printStackTrace();
			}
			List<List<AppProcess>> cacheSubset = null;
			cacheSubset = new ArrayList<>();
			if(to != null && from != null){
				 
			
				for(int i=from; i<to; i++){
					cacheSubset.add(cache.getSnapshotAtInstance(i));
				}
				
			}else{
				for(int i=0; i<cache.getCacheSize(); i++){
					cacheSubset.add(cache.getSnapshotAtInstance(i));
				}
			}
			request.setAttribute("SNAPSHOTS", cacheSubset);
			DebugUtil.log("TEST 2 " + cacheSubset.size());
			addInfoToMainPage(request);
			request.getRequestDispatcher("process_graph.jsp").forward(request, response);

			
		}
	}
	
	/**
	 * Metoda destinata citirii informatiilor necesare randarii paginilor. Informatiile sunt pastrate in instantele obiectelor clasei curente.
	 * Informatiile sunt adaugate pe request pentru forward-uri ulterioare
	 * @param request
	 */
	private void addInfoToMainPage(HttpServletRequest request){
		if(dbjob.getCache().getCacheSize() == 0){
			System.err.println("[Debug] Reading LIVE values");
			allProcesses = dao.DAOMemoryProcess.getAllProcesses();
		}else{
			System.err.println("[Debug] Reading CACHED verno: " + (dbjob.getCache().getCacheSize()-1));
			allProcesses = dbjob.getCache().getSnapshotAtInstance(dbjob.getCache().getCacheSize()-1); // primul snapshot (nu e live)
		}
		allServers = MSProperties.getMSSystemProps();
			
		request.setAttribute("CPU_CACHE",  dbjob.getCacheCPU());
		request.setAttribute("CACHE", dbjob.getCache());
		request.setAttribute("allProcesses", allProcesses);
		
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		DebugUtil.log("PATHU ESTE : " + request.getParameter("path"));
		
		
		if (request.getParameter("path").equals("security")) {
			if(request.getParameter("username").equals("admin")&& request.getParameter("password").equals("admin"))  {
				response.sendRedirect("testcomp/allcharts.jsp");
			}else {
				request.setAttribute("msg", "ACCESS DENIED");
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
			
		}else if (request.getParameter("path").trim().equals("computeroperation")) {
			String type = request.getParameter("type");
			String time = request.getParameter("time").trim();
			int value = Integer.parseInt(request.getParameter("number"));
		
			if(type.equals("shutdown")) {
				if(time.equals("hours")) {
					int valueofTime = value * 3600;
					cou.operation("shutdown -s -t " +valueofTime);
				}else if (time.equals("minutes")) {
					int valueOfTime = value*60;
					cou.operation("shutdown -s -t " +valueOfTime);
				}
				else if (time.equals("seconds")) {
					int valueOfTime=value;
					cou.operation("shutdown -s -t " +valueOfTime);
				}
				response.sendRedirect("ComputerOperation.jsp");
			}else if(type.equals("restart")) {
				if(time.equals("hours")) {
					int valueofTime = value * 3600;
					cou.operation("shutdown -r -t " +valueofTime);
			
				}else if (time.equals("minutes")) {
					int valueOfTime = value*60;
					cou.operation("shutdown -r -t " +valueOfTime);
				}
				else if (time.equals("seconds")) {
					int valueOfTime=value;
					cou.operation("shutdown -r -t " +valueOfTime);
				}
				response.sendRedirect("ComputerOperation.jsp");
			}
	
		
	
		}else if(request.getParameter("path").equals("abort")) {
			cou.operation("shutdown -a");
			response.sendRedirect("ComputerOperation.jsp");
		
		}else if(request.getParameter("path").equals("forceclose")) {
			
			String[]selectedProcesses = request.getParameterValues("process");
			
			for(String s : selectedProcesses) {
				cou.taskKill(s.trim());
			}
			
			response.sendRedirect("SecurityServlet?path=security");
		}
			
		
}
}