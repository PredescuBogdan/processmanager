package util;

import java.util.List;

import model.processes.AppProcess;

public class JSONUtil {

	/**
	 * Metoda care poate transforma o lista de liste de procese intr-un obiect JSON
	 * @param cacheSubsetj
	 * @return
	 */
	public static String toJSON(List<List<AppProcess>> cacheSubset) {
		String snapshotJSON = "";
		int nr = 0;
		snapshotJSON += "[";
		for (List<AppProcess> aps : cacheSubset) {
			snapshotJSON += "{'snapshot_no' : '" + nr++ + "', 'processes' : ";
			snapshotJSON += "[";

			for (AppProcess app : aps) {
				snapshotJSON += app.toJSON() + " ,";

			}
			snapshotJSON = snapshotJSON.substring(0, snapshotJSON.length() - 1);
			snapshotJSON += "]";
			snapshotJSON += "},";

		}
		snapshotJSON = snapshotJSON.substring(0, snapshotJSON.length() - 1);
		snapshotJSON += "]";
		return snapshotJSON;
	}
}
