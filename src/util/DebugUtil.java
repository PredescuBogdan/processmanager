package util;

import java.util.Date;

public class DebugUtil {

	/**
	 * Metoda destinata printarii user-friendly a informatiilor de debug (destinata programatorilor)
	 * @param message mesajul ce va fi afisat
	 */
	public static void log(String message){
		System.err.println("[Debug] " + message + " ["+(new Date().toString())+ "]");
	}
}
