package util;

import java.io.IOException;

public class ComputerOperationUtil {
	
	/**
	 * Executare operatie in command prompt
	 * @param command instanta String reprezentand comanda ce va fi executata
	 */
	public void operation(String command) {
		try {
			Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Metoda ce realizeaza oprirea unui proces prin intermediul comenzii taskkill
	 * @param process numele procesului ce urmeaza a fi oprit
	 */
	public void taskKill(String process) {
		try {
			Runtime.getRuntime().exec("taskkill /F /S localhost /IM " +process);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
