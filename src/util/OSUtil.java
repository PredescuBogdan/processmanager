package util;

import java.util.Iterator;
import java.util.Map;

public class OSUtil {
	
	

	/**
	 * Metoda destinata debug-ului, printeaza elementele unei instante de tip Map
	 * @param mp instanta a carei informatii vor fi afisate
	 */
	public static void printMap(Map mp) {
		Iterator it = mp.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			System.out.println("     \t\t"+pair.getKey() + " = " + pair.getValue());
		}
	}
	
	
}
