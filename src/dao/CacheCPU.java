package dao;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import util.DebugUtil;

public class CacheCPU {

	/**
	 * Variabila cpuValues este de tip list. De fiecare data cand este adaugata o informatie referitoare la gradul de incarcqare al procesorului
	 * informatia este stocata aici 
	 */
	private List<Integer> cpuValues;
	
	public CacheCPU(){
		cpuValues = new ArrayList<>();
	}
	
	/**
	 * Metoda pentru preluarea informatiilor referitoare la cat de incarcat este procesorul in momentul apelarii
	 * @return
	 */
	public static Integer getCPUUsageNOW() {
		Integer usage = null;
		try {
			String line;
			Process p = Runtime.getRuntime().exec("wmic cpu get loadpercentage");

			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			List<String> liniiDinComanda = new ArrayList<>();
			while ((line = input.readLine()) != null) {
				liniiDinComanda.add(line);
			}
			usage = Integer.parseInt(liniiDinComanda.get(2).trim());
			input.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
		return usage;
	}
	
	
	/**
	 * Metoda adauga in cpuValues un nou entry corespunzator cu gradul de incarcare actual al procesorului 
	 */
	public void refreshCPUCache(){
		DebugUtil.log("LOGGING CPU PERFORMANCE " + cpuValues);
		cpuValues.add(getCPUUsageNOW());
	}
	
	public List<Integer> getCpuValues() {
		return cpuValues;
	}


	public void setCpuValues(List<Integer> cpuValues) {
		this.cpuValues = cpuValues;
	}


}
