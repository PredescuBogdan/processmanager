package dao;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import model.processes.AppProcess;

/**
 * Clasa ce incapsuleaza logica de preluare a informatiilor referitoare la procesele sistemului
 * @author Bogdan
 *
 */
public class DAOMemoryProcess {

	private static Integer getUsageAsNumberKB(String usage) { //metoda statica ce returneaza in kb cpu load ul
		usage = usage.replace(",", "");
		Integer result = Integer.parseInt(usage);
		return result;
	}

	/**
	 * Metoda destinata incarcarii informatiilor specifice tuturor proceselor ce lucreaza pe calculator in momentul apelarii metodei. 
	 * Metoda este specifica sistemului de operare Microsoft Windows 
	 * @return List<AppProcess> o lista ce contine modelul specific
	 */
	public static List<AppProcess> getAllProcesses() { //metoda statica ce returneaza o lista de procese 
		List<AppProcess> processes = new ArrayList<>(); 
		try {
			String line;
			Process p = Runtime.getRuntime().exec("tasklist.exe /nh");

			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream())); //citire din cmd
			while ((line = input.readLine()) != null) { 
				if (!line.trim().equals("")) {
					AppProcess ap = parseEntry(line);
					processes.add(ap);
				}
			}
			input.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
		return processes;
	}

	/**
	 * 
	 * @param line obiect de tip String reprezentand informatia returnata pe o linie de comanda <code>tasklist.exe /nh</code>
	 * Informatia este transformata dintr-un String intr-o instanta AppProcess
	 * @return obiect de tip AppProcess utilizat pentru incapsularea informatiilor intr-o maniera OOP
	 */
	private static AppProcess parseEntry(String line) {

		// Image Name PID Session Name Session# Mem Usage
		// ========================= ======== ================ ===========
		// ============
		// System Idle Process 0 Services 0 4 K

		AppProcess prc = null;

		int firstIndexOfSpace = line.indexOf("  ");
		String pName = line.substring(0, firstIndexOfSpace);

		String remainingString = line.substring(firstIndexOfSpace);

		remainingString = remainingString.trim();
		String elements[] = remainingString.split(" ");
		List<String> correctElements = new ArrayList<>();
		for (String e : elements) {
			if (!e.contains(" ") && !e.isEmpty()) {
				correctElements.add(e);

			}
		}
		prc = new AppProcess(pName, Integer.parseInt(correctElements.get(0)), correctElements.get(1),
				Integer.parseInt(correctElements.get(2)), getUsageAsNumberKB(correctElements.get(3)));

		return prc;
	}

	/**
	 * Metoda apeleaza <code>tasklist.exe /nh</code> si afiseaza informatiile linie cu linie
	 * Serveste scopurilor de debug
	 * @return
	 */
	public static List<String> listRunningProcesses() {

		List<String> processes = new ArrayList<String>();
		try {
			String line;
			Process p = Runtime.getRuntime().exec("tasklist.exe /nh");

			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				if (!line.trim().equals("")) {
					// keep only the process name
					int firstIndexOfSpace = line.indexOf("  ");

					String pName = line.substring(0, firstIndexOfSpace);
//					parseEntry(line);
					processes.add(pName);
				}
			}
			input.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
		return processes;
	}
	
	public static void main(String[] args) {
		System.out.println(listRunningProcesses());
		System.out.println("===============1");
		System.out.println(getAllProcesses());
	}

}
