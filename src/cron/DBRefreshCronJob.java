package cron;

import java.util.Timer;
import java.util.TimerTask;

import dao.CacheCPU;
import dao.DAOMemoryProcess;
import db.util.CacheDBProcess;
import db.util.ScriptUtil;

/**
 * @author Bogdan
 * CACHES are located here, cache - snapshots of database process table
 * cacheCPU - CPU usage at UPDATE_TIME_INTERVAL seconds
 *
 */
public class DBRefreshCronJob {

	private Timer timer;
	private ScriptUtil su;
	private DAOMemoryProcess daoMemory;
	
	private CacheDBProcess cacheProcesses;
	private CacheCPU cacheCPU;
	
	private static final int UPDATE_TIME_INTERVAL = 6000;
	
	public static final Boolean DEBUG = false;
	
	
	
	
	public CacheDBProcess getCache() {
		return cacheProcesses;
	}

	public void setCache(CacheDBProcess cache) {
		this.cacheProcesses = cache;
	}
	
	public CacheCPU getCacheCPU() {
		return cacheCPU;
	}

	public void setCacheCPU(CacheCPU cacheCPU) {
		this.cacheCPU = cacheCPU;
	}

	public DBRefreshCronJob(DAOMemoryProcess dao){
		this.daoMemory = dao;
		cacheProcesses = new CacheDBProcess();
		cacheCPU = new CacheCPU();
		timer = new Timer();
		su = new ScriptUtil();
		su.cleanDBDROP();
		su.generateDBScriptsComputers(true);
		su.generateScriptsProcesses(true);
	}
	
	/**
	 * Metoda destinata pornirii CRON-ului
	 * rezulta in crearea unui obiect de tip RefreshDBUtil
	 */
	public void startDBandCPURefreshing(){
		RefreshDBUtil rdb = new RefreshDBUtil();
		
		timer.schedule(rdb, 0, UPDATE_TIME_INTERVAL);
		
	}
	
	public void stopDBRefreshing(){
		timer.cancel();
	}
	
	/**
	 * Clasa interna, folosita numai in interiorul lui DBRefreshCronJob
	 * @author Bogdan
	 *
	 */
	class RefreshDBUtil extends TimerTask{

		private int numberOfRefreshes = 0;
		
	
		/**
		 * De fiecare data cand ruleaza acest task, 
		 * 		este incrementat numarul de informatii referitoare la incarcarea CPU-ului
		 * 		este adaugat un Snapshot la cacheProcesses
		 * 			un snapshot reprezinta starea tuturor proceselor la timpul t
		 */
		@Override
		public void run() {
			cacheCPU.refreshCPUCache();
			numberOfRefreshes++;
			su.cleanDBDelete();
			
			su.generateDBScriptsComputers(false);
			su.generateScriptsProcesses(false);
			
			cacheProcesses.addDbSnapshot(daoMemory.getAllProcesses());
			if(DEBUG)
				System.out.println(cacheProcesses); 
		}
		
	}
	
	

	public static void main(String[] args) {
		DBRefreshCronJob dbjob = new DBRefreshCronJob(new DAOMemoryProcess());
		dbjob.startDBandCPURefreshing();
		
	}
}
