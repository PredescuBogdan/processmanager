<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Computer Operation</title>
</head>
<body>
	
	<form action="SecurityServlet?path=computeroperation" method="post">
		Type<br>
		<input type="radio" name="type" value="shutdown" checked>Shutdown<br>
		<input type="radio" name="type" value="restart">Restart<br>
		
		Time<br>
		<input type = "radio" name = "time" value="seconds">Seconds<br>
		<input type = "radio" name = "time" value="minutes">Minutes<br>
		<input type = "radio" name = "time" value="hours" checked>Hours<br>
		<input type ="text" name="number"><br><br>
		<input type = "submit" value="Submit">
	</form>
	<form action="SecurityServlet?path=abort" method="post">
		<input type = "submit" value="Abort Operation">
	</form>
	
</html>