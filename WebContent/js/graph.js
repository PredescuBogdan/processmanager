/**
 * Functie ce genereaza un numar aleator intre 0 si 255
 * folosit la crearea de culori
 */ 
var randomColorFactor = function() {
            return Math.round(Math.random() * 255);
        };
        
        
        /**
         * Metoda destinata generarii unei culori random
         */
        var randomColor = function(opacity) {
            return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';  // if not defined / null opacity => .3
        };
        
        /**
         * Metoda destinata generarii unei culori 'standard'
         */
        var steadyColor = function(opacity) {
            return 'rgba(' + 120 + ',' + 50 + ',' + 99 + ',' + (opacity || '.3') + ')';  // if not defined / null opacity => .3
        };
        
        
		
        
        /**
         * 
         * @param labelsDown informatia prezentata ca subscript pentru axa x
         * @param valuesGraph Valorile in fiecare punct sub forma de array
         * @param labelX eticheta axei X
         * @param labelY eticheta axei Y
         * @param graphTitle titlul graficului 
         * @param limitSize numarul de elemente
         * @returns 
         */
        function generateConfig(labelsDown, valuesGraph, labelX, labelY, graphTitle, limitSize){
        	
        	
            function limitConfigValues(config, limitSize){
            	
            	var valoriAfisate = config.data.datasets[0].data;
                if(valoriAfisate.length > 53){
        	        console.log('afisam: ' + valoriAfisate.length + " valori");
        	        var subsetDeValoriAfisate = valoriAfisate.slice(valoriAfisate.length - limitSize, valoriAfisate.length);  // luam ultimele elemente (i.e. cele mai noi masuratori)
        	        config.data.datasets[0].data = subsetDeValoriAfisate;
        	        console.log("labeluri: ");
        	        console.log(config.data.labels);
        	        config.data.labels = config.data.labels.slice(config.data.labels.length - limitSize, config.data.labels.length);
                }	
            }
        	
        	var config = {
                    type: 'line',
                    data: {
                        labels:  labelsDown,
                        datasets: [{
                            label: "CPU VALUES",
                            data: valuesGraph,
                            fill: false,
                            borderDash: [5, 5],
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'bottom',
                        },
                        hover: {
                            mode: 'label'
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: labelX
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: labelY
                                }
                            }]
                        },
                        title: {
                            display: true,
                            text: graphTitle
                        }
                    }
                };
        	
        	
        	limitConfigValues(config, limitSize);
        	return config;
        	
        	
        } // end generateConfig()
        
        
        
 function generateConfigWithMultipleDatasets(labelsDown, rows, labelX, labelY, graphTitle, limitSize){
        	
        	
            function limitConfigValues(config, limitSize){
            	
            	var valoriAfisate = config.data.datasets[0].data;
                if(valoriAfisate.length > 53){
        	        console.log('afisam: ' + valoriAfisate.length + " valori");
        	        var subsetDeValoriAfisate = valoriAfisate.slice(valoriAfisate.length - limitSize, valoriAfisate.length);
        	        config.data.datasets[0].data = subsetDeValoriAfisate;
        	        console.log("labeluri: ");
        	        console.log(config.data.labels);
        	        config.data.labels = config.data.labels.slice(config.data.labels.length - limitSize, config.data.labels.length);
                }	
            }
        	
        	var config = {
                    type: 'line',
                    data: {
                        labels:  labelsDown,
                        datasets: rows
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'bottom',
                        },
                        hover: {
                            mode: 'label'
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: labelX
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: labelY
                                }
                            }]
                        },
                        title: {
                            display: true,
                            text: graphTitle
                        }
                    }
                };
        	
        	
        	limitConfigValues(config, limitSize);
        	return config;
        	
        	
        } // end generateConfig()
        