<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.HashMap" %>
<%@ include file="../components/libs.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Computer Details</title>
</head>
<script>
	$(document).ready(function() {
	    $('table.display').DataTable();
	} );
	</script>

<body>
	
	<%
		if(request.getAttribute("ComputerInfo") != null) {
			
			HashMap<String,String> compDetails = (HashMap<String,String>) request.getAttribute("ComputerInfo");
			out.println(
					"<table style='width:50%' border='1px solid black' class='display'><tr><th>Name</th><th>Value</th></tr>");
			
			for (String key : compDetails.keySet()) {
			    
				out.print("<tr><td>" + key + "</td><td>" + compDetails.get(key) + "</td></tr>");
			}
			
		}
	%>
</body>
</html>