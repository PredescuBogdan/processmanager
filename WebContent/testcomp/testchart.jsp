<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="model.processes.AppProcess"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="dao.CacheCPU"%>

<html>

<head>
<title>Line Chart</title>
<%@ include file="../components/libs.jsp"%>
<script src="<%=globalPath%>/js/graph.js"></script>
<script>
	$(document).ready(function() {

		
		// 
		var timeout = setTimeout(function() { // folosesc functia setTimeout pentru a face refresh pe pagina
			
			window.location.reload(1);
		}, 5000);

		$('#dialog').hide();

		$('#printStats').click(function() {
			clearTimeout(timeout); // functia clearTimeout din JS

			$("#dialog").dialog({  // elemeentul (div-ul) cu id-ul 'dialog' este transformat intr-un element grafic UI
				buttons : [ { // parametrii sunt de tip JSON
					text : "OK",
					click : function() {
						$(this).dialog("close");
						timeout = setTimeout(function() {  // reactivam timeout-ul
							window.location.reload(1);
						}, 5000);
					}
				} ]
			});
		});

	});
</script>
</head>

<body>
	<%
		// cpu
		CacheCPU cpuCache = (CacheCPU) request.getAttribute("CPU_CACHE");
		List<Integer> cpuValues = cpuCache.getCpuValues();
		// /cpu
	%>
	<div style="width: 100%;">
		<canvas id="canvas"></canvas>
	</div>
	<br>
	<br>

	<br>
	<script>
		
	<%
			List<Integer> values = new ArrayList<>();
			for (int i = 0; i < cpuValues.size(); i++) {
				values.add(i);
			}%>
		var config = generateConfig(
	<%=values%>
		,
	<%=cpuValues%>
		,
				'measurement no', 'processor load [%]', 'CPU LOAD Graph', 50);

		$.each(config.data.datasets, function(i, dataset) {
			var background = steadyColor(0.5);
			dataset.borderColor = background;
			dataset.backgroundColor = background;
			dataset.pointBorderColor = background;
			dataset.pointBackgroundColor = background;
			dataset.pointBorderWidth = 1;
		});

		window.onload = function() {
			var ctx = document.getElementById("canvas").getContext("2d");
			window.myLine = new Chart(ctx, config);
		};


	</script>

	<div id="dialog" title="Debug information">
		<p>
			CPU LOAD VALUES:
			<%=cpuValues%></p>
	</div>
	<button id="printStats">View debug info</button>
</body>

</html>
