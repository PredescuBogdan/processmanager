<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="model.processes.AppProcess"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="dao.CacheCPU"%>

<html>

<head>
<title>OS Project</title>
<%@ include file="../components/libs.jsp"%>
<script src="<%=globalPath%>/js/graph.js"></script>

<script>
	$(document).ready(function() {
		$("#tabs").tabs();
	});
</script>
</head>


<body>

	<div id="tabs">
		<ul>
			<li><a href="#tabs-1">Process Management</a></li>
			<li><a href="#tabs-2">CPU Stats</a></li>
			<li><a href="#tabs-3">Computer Details</a></li>
			<li><a href="#tabs-4">Computer Schedule</a></li>
			<li><a href="#tabs-5">About</a></li>
			
		</ul>
		<div id="tabs-1">
			<p>
				<iframe
					src="http://localhost:8080/ProiectOS/SecurityServlet?path=security"
					style="height: 800px; width: 1000px;">
					<p>Your browser does not support iframes.</p>
				</iframe>
			</p>
		</div>
		<div id="tabs-2">
			<p>
				<iframe
					src="http://localhost:8080/ProiectOS/SecurityServlet?path=testchart"
					style="height: 800px; width: 1000px;">
					<p>Your browser does not support iframes.</p>
				</iframe>
			</p>
		</div>
		<div id="tabs-3">
			<p>
				<iframe
					src="http://localhost:8080/ProiectOS/SecurityServlet?path=compdetails"
					style="height: 800px; width: 1000px;">
					<p>Your browser does not support iframes.</p>
				</iframe>
			</p>
		</div>
		<div id="tabs-4">
			<p>
				<iframe
					src="http://localhost:8080/ProiectOS/ComputerOperation.jsp"
					style="height: 800px; width: 1000px;">
					<p>Your bsrowser does not support iframes.</p>
				</iframe>
			</p>
		</div>
		<div id="tabs-5">
			<h1>Task Manager</h1>
			<h1>OS - Project </h1>
			<p>&copy; Bogdan Predescu</p>
			<p>&copy; Ana-Maria Raluca Chiru</p>
			<p>  <a href="../resources/UserManual.pdf" target="_blank"><img src="../resources/userm.jpg" alt="UserManual" height="100" width="100" align="center"/></a></p>
			<p>  <a href="../resources/Documentation.pdf" target="_blank"><img src="../resources/doc.png" alt="Documentation" height="100" width="140" align="center"/></a></p>
			<p>  <a href="../resources/doc/index.html" target="_blank">JavaDoc</a></p>
			
		</div>
	</div>



</body>

</html>
