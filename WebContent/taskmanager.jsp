<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="model.processes.AppProcess"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="db.util.CacheDBProcess"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Processes</title>

</head>
<body>
	<%@ include file="../components/libs.jsp"%>
	<h2>Task Manager</h2>

	<form action="SecurityServlet?path=forceclose" method="post">


		<%
			
			if (request.getAttribute("msg") != null) {
				out.println("<p style='color:green'>" + request.getAttribute("msg") + "</p>");
			}

			if (request.getAttribute("allProcesses") != null) {
				List<AppProcess> allProcesses = (List<AppProcess>) request.getAttribute("allProcesses");
				out.println("<h3>PROCESSES</h3>");
				out.println(
						"<table style='width:50%' border='1px solid black' class='display'><tr><th>Image Name</th><th>PID</th><th>Mem Ocuppied(kb)</th></tr>");

				for (AppProcess ap : allProcesses) {
					out.print("<tr><td><input type='checkbox' name='process' value='" + ap.getImageName() + "'>"
							+ ap.getImageName() + "</td><td>" + ap.getPid() + "</td><td>" + ap.getMemUsageKB()
							+ "</td>");
					out.print("</tr>");

				}
			}
		%>

		<input type="submit" value="Force Close">
	</form>


	<script>
	$(document).ready(function() {
	    $('table.display').DataTable();
	} );
	</script>

</body>
</html>